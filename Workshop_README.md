# PACT Workshop


## Prerequisites 
* Docker must be installed and running
* npm must be installed and running

## Setup
May need admin/sudo to run these depending on how your system is set up.

Build with the node package manager's (npm) install command in the working directory.

```
$ npm i
```

Now start the pact broker, a server that will run on port 8080 as a docker container.
```
$ npm run pact-broker
```

You can check that the broker is running in docker. Also, take a look in your browser at http://localhost:8080. This is the PACT interface. It is currently showing one set of contracts for the Zoo app and Animal service.

In this workshop we will create and publish a new pact for Client (consumer) and ProductService (provider) service to this dashboard.

## Before we get started
This workshop includes a set of consumer contract tests and a set of provider contract tests. The commands below will help you become aquanted with this suite.

### To run the consumer tests:
```
npm run test-consumer
```
Uses Mocha and Chai. All tests should pass.

### To publish our contract tests (pacts) to our pact broker:
```
npm run publish-pacts
```
Once you have run this, check the pact dashboard http://localhost:8080 and you should see a new pact for Client and ProductService. 


### To run the provider tests:
```
npm run test-provider
```
__Note:__ You must publish your pacts before this suite of tests will run and pass.

## Exercise: Create a pact for getting a product by id

### 1. Add test in the client spec.
./client/client.spec.js
```
describe('#getById', () => {
  it('should return product based on id', async function () {
    await provider.addInteraction(mocks.getById)
    const response = await client.getById(1)

    const expectedBody = {
      name: 'Bar',
      img: 'https://webshop.com/img/cheap-shoe.png',
      price: 2,
      stock: 3,
      id:1
    }

    expect(response).to.be.eql(expectedBody)

    await provider.verify()
  })
})
```

### 2. In mocks.js, add the following interaction after registerProduct (line 84)
./client/expectation/mocks.js

```
,
getById: {
  state: 'a product with id of 1 is created',
  uponReceiving: 'a request to retrieve product by id',
  withRequest: {
    method: 'GET',
    path: '/products/1'
  },
  willRespondWith: {
    status: 200,
    body: PRODUCT_ID_BODY
  }
}
```

### 3. Run the consumer tests
```
$ npm run test-consumer
```
You should get the following:
```
CLIENT: Current products are: Foo
      ✓ should get product list from server
    #getProducts
      ✓ should return product list based on query
    #registerProduct
      ✓ should send product registration request
    #getById
      ✓ should return product based on id


  4 passing (1s)

```

You'll see that client-productservice.json has been updated with the new contract.

```
    {
      "description": "a request to retrieve product by id",
      "providerState": "a product with id of 1 is created",
      "request": {
        "method": "GET",
        "path": "/products/1"
      },
      "response": {
        "status": 200,
        "headers": {
        },
        "body": {
          "id": 1,
          "name": "Bar",
          "img": "https://webshop.com/img/cheap-shoe.png",
          "price": 2,
          "stock": 3
        },
        "matchingRules": {
          "$.body.id": {
            "match": "type"
          }
        }
      }
    }
```

### 4. Run the publisher to publish the new contract. 
```
npm run publish-pacts
```

Go to http://localhost:8080/ <br>
* You should see that the "Last pact published" for the ProductService  was updated "less than a minute ago"

Click on the page icon in the row and you should see the new contract:
```
Given a product with id of 1 is created, upon receiving a request to retrieve product by id from Client, with

{
  "method": "GET",
  "path": "/products/1"
}

ProductService will respond with:

{
  "status": 200,
  "body": {
    "id": 1,
    "name": "Bar",
    "img": "https://webshop.com/img/cheap-shoe.png",
    "price": 2,
    "stock": 3
  }
}
```

### 5. Add the new test to the provider tests in testProductsService.js by adding a new case to the switch statement
./server/consumerTests/testProductsService.js

```
case 'a product with id of 1 is created':
  products._flush()
  products.create({ name: 'Bar', img: 'https://webshop.com/img/cheap-shoe.png', price: 2, stock: 3})
  break;
```

### 6. Finally, run the provider tests
```
npm run test-provider
```


## Challenge / Extra Credit:
Create a new PACT test for deleting an product

Extra credit: Create a new PACT test for updating a product
